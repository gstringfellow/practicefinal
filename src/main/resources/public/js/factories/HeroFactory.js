'use strict';
angular.module("HeroApp").factory("HeroFactory",['$http',function($http){
    return{
    heroes:$http.get('/hero')
    };
    }
])