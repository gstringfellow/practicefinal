angular.module('HeroApp').controller("HeroDisplayController",['$scope','HeroFactory',function($scope,HeroFactory){
    HeroFactory.heroes.then(function(s){$scope.heroes = s.data});
}]);