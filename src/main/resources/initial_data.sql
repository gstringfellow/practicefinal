INSERT INTO powertype (name) VALUES ('movement');
INSERT INTO powertype (name) VALUES ('offensive');
INSERT INTO powertype (name) VALUES ('defensive');
INSERT INTO powertype (name) VALUES ('type1');
INSERT INTO powertype (name) VALUES ('type2');

INSERT INTO power (name, description, type_id) VALUES ('super strength', 'Stronger than your average person', 2);
INSERT INTO power (name, description, type_id) VALUES ('Flying', 'It''s a bird, it''s a plane, no it''s...', 1);
INSERT INTO power (name, description, type_id) VALUES ('invisibility', 'Where''d they go?', 3);
INSERT INTO power (name, description, type_id) VALUES ('power1', 'desc1', 2);
INSERT INTO power (name, description, type_id) VALUES ('power2', 'desc2', 4);
INSERT INTO power (name, description, type_id) VALUES ('power3', 'desc3', 5);


INSERT INTO superhero (name, height, weight, imageurl, origin, realname) VALUES ('spiderman', '72', '150', 'www.image.com', 'spiderbite', 'peter parker');
INSERT INTO superhero (name, height, weight, imageurl, origin, realname) VALUES ('hero1', '72', '150', 'www.image.com', 'origin1', 'realname1');
INSERT INTO superhero (name, height, weight, imageurl, origin, realname) VALUES ('hero2', '72', '150', 'www.image.com', 'origin2', 'realname2');

INSERT INTO team (name, headquarters, supportlevel, leader_id) VALUES ('team1', 'Dereks mom', '3', 1);

INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (1, 1, 3);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (1, 4, 2);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (1, 5, 6);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (2, 1, 2);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (2, 2, 5);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (2, 3, 7);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (3, 5, 9);
INSERT INTO superhero_power (superheroid, powerid, powerrating) VALUES (3, 4, 7);

INSERT INTO team_superhero (team_id, members_id) VALUES (1, 1);
INSERT INTO team_superhero (team_id, members_id) VALUES (1, 2);
INSERT INTO team_superhero (team_id, members_id) VALUES (1, 3);