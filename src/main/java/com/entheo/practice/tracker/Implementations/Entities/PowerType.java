package com.entheo.practice.tracker.Implementations.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Entity
public class PowerType extends BaseEntity {
    @Column(nullable = false)
    private String type;
    private String description;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PowerType{" +
                "id='" + id + "\', " +
                "type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
