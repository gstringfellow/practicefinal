package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Entities.PowerType;
import com.entheo.practice.tracker.Interfaces.Dao.IPowerTypeDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
@Transactional
public class PowerTypeDao extends BaseDao<PowerType> implements IPowerTypeDao{
    public PowerTypeDao() {
        super("PowerType",PowerType.class);
    }
}
