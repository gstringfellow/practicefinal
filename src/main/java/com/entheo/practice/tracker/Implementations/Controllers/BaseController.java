package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Data;
import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;
import com.entheo.practice.tracker.Implementations.Entities.Hero;
import com.entheo.practice.tracker.Implementations.Services.BaseService;
import com.entheo.practice.tracker.Interfaces.IBaseCommands;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RestController
public abstract class BaseController<T extends BaseEntity> implements IBaseCommands<T> {

    protected BaseService<T> service;

    public BaseController(BaseService service) {
        this.service = service;
    }


    @RequestMapping(value="", method=RequestMethod.GET)
    public ArrayList<T> getAll() {
        System.out.println("hello");
        return service.getAll();
    }

    @RequestMapping(value="", method=RequestMethod.POST)
    public void add(@RequestBody T t) {
        service.add(t);
    }

    @RequestMapping(value="", method=RequestMethod.PUT)
    public void update(@RequestBody T t) {
        service.update(t);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.POST)
    public T getByID(@PathVariable Integer id) {
        return (T) service.getByID(id);
    }
}
