package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Entities.Origin;
import com.entheo.practice.tracker.Implementations.Services.OriginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RequestMapping(value = "/origin")
@RestController
public class OriginTypeController extends BaseController<Origin>{
    @Autowired
    public OriginTypeController(OriginService service) {
        super(service);
    }
}
