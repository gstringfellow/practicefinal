package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Entities.Costume;
import com.entheo.practice.tracker.Implementations.Services.CostumeService;
import com.entheo.practice.tracker.Interfaces.IBaseCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RequestMapping(value = "/costume")
@RestController
//if you want to override a function with logic must use this>>>
public class CostumeController extends BaseController<Costume> implements IBaseCommands<Costume> {
    @Autowired
    public CostumeController(CostumeService service) {
        super(service);
    }
}
