package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Daos.PowerDao;
import com.entheo.practice.tracker.Interfaces.Dao.IPowerDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.Power;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service

public class PowerService extends BaseService<Power> {
    @Autowired
    public PowerService(IPowerDao dao) {
        super(dao);
    }
}
