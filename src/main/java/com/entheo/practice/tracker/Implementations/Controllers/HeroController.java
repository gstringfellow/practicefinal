package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Data;
import com.entheo.practice.tracker.Implementations.Entities.Hero;
import com.entheo.practice.tracker.Implementations.Services.HeroService;
import com.entheo.practice.tracker.Interfaces.IBaseCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */

@RestController
@RequestMapping(value = "/hero")
public class HeroController extends BaseController<Hero>{
    private HeroService service;
    @Autowired
    public HeroController(HeroService service) {
        super(service);
        this.service = service;
        for(Hero h: Data.getHeroes())
            add(h);
    }

    @RequestMapping(value="/{heroName}", method=RequestMethod.GET)
    public Hero getByHeroName(@PathVariable String heroName) {

        return service.getByHeroName(heroName);
    }

    @RequestMapping(value="/{mundaneName}", method=RequestMethod.GET)
    public Hero getByMundaneName(@PathVariable String mundaneName) {
        return service.getByHeroName(mundaneName);
    }



}
