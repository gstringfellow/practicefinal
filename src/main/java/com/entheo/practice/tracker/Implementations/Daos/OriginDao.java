package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Entities.Origin;
import com.entheo.practice.tracker.Interfaces.Dao.IOriginDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
@Transactional
public class OriginDao extends BaseDao<Origin> implements IOriginDao{
    public OriginDao() {
        super("Origin",Origin.class);
    }

}
