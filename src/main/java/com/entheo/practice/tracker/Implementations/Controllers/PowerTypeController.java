package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Entities.PowerType;
import com.entheo.practice.tracker.Implementations.Services.PowerTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RequestMapping(value = "/powertype")
@RestController
public class PowerTypeController extends BaseController<PowerType> {
    @Autowired
    public PowerTypeController(PowerTypeService service) {
        super(service);
    }
}
