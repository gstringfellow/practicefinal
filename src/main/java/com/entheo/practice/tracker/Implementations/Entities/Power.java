package com.entheo.practice.tracker.Implementations.Entities;

import com.entheo.practice.tracker.Interfaces.IPersistableChildren;

import javax.persistence.*;
import java.util.ArrayList;

/**
 {
 "name":"nameything","description":"descripy",
 "powerType":{"type" : "bangboom", "description" : "explody"}

 }

 */
@Entity
public class Power extends BaseEntity implements IPersistableChildren {

    @Override
    public String toString() {
        return "Power{" +
                "id='" + id + "\', " +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", powerType=" + powerType +
                '}';
    }

    @Column(nullable = false)
    private String name;
    private String description;
    @ManyToOne
    private PowerType powerType;

    public ArrayList<BaseEntity> childrenToPersist() {
        ArrayList<BaseEntity> a = new ArrayList<BaseEntity>();
        if(powerType != null && !powerType.isReference()) {
            a.add(powerType);
            return a;
        }
        return a;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PowerType getPowerType() {
        return powerType;
    }

    public void setPowerType(PowerType powerType) {
        this.powerType = powerType;
    }
}
