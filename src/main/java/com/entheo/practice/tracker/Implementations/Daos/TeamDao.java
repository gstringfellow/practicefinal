package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Entities.Team;
import com.entheo.practice.tracker.Interfaces.Dao.ITeamDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
@Transactional
public class TeamDao extends BaseDao<Team> implements ITeamDao{
    public TeamDao() {
        super("Team",Team.class);
    }
}
