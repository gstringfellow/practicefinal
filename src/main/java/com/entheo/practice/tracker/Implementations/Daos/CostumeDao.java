package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Entities.Costume;
import com.entheo.practice.tracker.Interfaces.Dao.ICostumeDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
public class CostumeDao extends BaseDao<Costume> implements ICostumeDao {

    public CostumeDao() {
        super("Costume",Costume.class);

    }
}
