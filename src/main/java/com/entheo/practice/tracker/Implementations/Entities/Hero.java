package com.entheo.practice.tracker.Implementations.Entities;

import com.entheo.practice.tracker.Interfaces.IPersistableChildren;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

/**
 {
 "heroName":"SuperMan",
 "mundaneName":"Clark",
 "origin": {"origin":"Krypton", "description":"green"},
 "costume": {"url":"CostumeURL"},
 "powers": [ {"name": "eyebeem", "description":"red", "powerType":{"type":"laser","description":"shoots"}},
 {"name": "strength", "description":"muscle", "powerType":{"type":"physical","description":"is"}}],
 "height":"215",
 "weight":"521"
 }

 {
 "heroName":"SuperMan",
 "mundaneName":"Clark",
 "origin": {"id":"1"},
 "costume": {"id":"1"},
 "powers": [ {"id":"1"},
 {"id":2}],
 "height":"215",
 "weight":"521"
 }

 {
 "heroName":"SuperMan",
 "mundaneName":"Clark",
 "origin": {"origin":"Krypton", "description":"green"},
 "costume": {"url":"CostumeURL"},
 "powers": [ {"id":"1"},
 {"id":2}],
 "height":"215",
 "weight":"521"
 }
 */
@Entity
public class Hero extends BaseEntity implements IPersistableChildren {
    @Column(nullable = false)
    private String heroName;
    @Column(nullable = false)
    private String mundaneName;
    @ManyToOne
    private Origin origin;
    @OneToOne
    private Costume costume;
    @ManyToMany()
    private Set<Power> powers;
    @Column(nullable = false)
    private int weight;
    @Column(nullable = false)
    private int height;

    public ArrayList<BaseEntity> childrenToPersist() {
        ArrayList<BaseEntity> persist = new ArrayList<BaseEntity>();
        if (origin != null && !origin.isReference())
            persist.add(origin);
        if(costume != null && !costume.isReference())
            persist.add(costume);
        for(Power p : powers)
            if(p != null && !p.isReference())
                persist.add(p);

        return persist;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "id='" + id + "\', " +
                "heroName='" + heroName + '\'' +
                ", mundaneName='" + mundaneName + '\'' +
                ", origin=" + origin +
                ", costume=" + costume +
                ", powers=" + powers +
                ", height=" + height +
                ", weight=" + weight +
                '}';
    }


    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public String getMundaneName() {
        return mundaneName;
    }

    public void setMundaneName(String mundaneName) {
        this.mundaneName = mundaneName;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin type) {
        this.origin = type;
    }

    public Costume getCostume() {
        return costume;
    }

    public void setCostume(Costume costume) {
        this.costume = costume;
    }

    public Set<Power> getPowers() {
        return powers;
    }

    public void setPowers(Set<Power> powers) {
        this.powers = powers;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
