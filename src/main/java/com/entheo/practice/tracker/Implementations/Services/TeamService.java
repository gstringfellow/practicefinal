package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Daos.TeamDao;
import com.entheo.practice.tracker.Interfaces.Dao.ITeamDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service

public class TeamService extends BaseService<Team> {
    @Autowired

    public TeamService(ITeamDao dao) {
        super(dao);
    }
}
