package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Entities.Power;
import com.entheo.practice.tracker.Implementations.Services.PowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RequestMapping(value = "/power")
@RestController
public class PowerController extends BaseController<Power>{
    @Autowired
    public PowerController(PowerService service) {
        super(service);
    }
}
