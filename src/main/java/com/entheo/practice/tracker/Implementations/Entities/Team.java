package com.entheo.practice.tracker.Implementations.Entities;

import com.entheo.practice.tracker.Interfaces.IPersistableChildren;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Entity
public class Team extends BaseEntity implements IPersistableChildren {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String headQuarters;
    @OneToOne
    private Hero teamLeader;


    @OneToMany
    private Set<Hero> teamMembers;
    @Column(nullable = false)
    private int supportLevel;

    public ArrayList<BaseEntity> childrenToPersist() {
        ArrayList<BaseEntity> a = new ArrayList<BaseEntity>();
        if(teamLeader != null && teamLeader.isReference())
            a.add(teamLeader);
        for(BaseEntity e: teamMembers)
            if(e != null && e.isReference()) {
                System.out.println(e);
                a.add(e);
            }
        return a;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id='" + id + "\', " +
                "name='" + name + '\'' +
                ", headQuarters='" + headQuarters + '\'' +
                ", teamLeader=" + teamLeader +
                ", teamMembers=" + teamMembers +
                ", supportLevel=" + supportLevel +
                '}';
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadQuarters() {
        return headQuarters;
    }

    public void setHeadQuarters(String headQuarters) {
        this.headQuarters = headQuarters;
    }

    public Hero getTeamLeader() {
        return teamLeader;
    }

    public void setTeamLeader(Hero teamLeader) {
        this.teamLeader = teamLeader;
    }

    public Set<Hero> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(Set<Hero> teamMembers) {
        this.teamMembers = teamMembers;
    }

    public int getSupportLevel() {
        return supportLevel;
    }

    public void setSupportLevel(int supportLevel) {
        this.supportLevel = supportLevel;
    }

}
