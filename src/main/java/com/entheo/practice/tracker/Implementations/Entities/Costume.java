package com.entheo.practice.tracker.Implementations.Entities;

import javax.persistence.Entity;

/**
 {
    "url":"costumeURL"
 }

 */


@Entity
public class Costume extends BaseEntity {

    private String url;

    public Costume(int id, String newUrl) {
        super();
        setId(id);
        setUrl(newUrl);
    }

    public Costume() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Costume{" +
                "id='" + getId() + '\'' +
                "url='" + url + '\'' +
                '}';
    }
}
