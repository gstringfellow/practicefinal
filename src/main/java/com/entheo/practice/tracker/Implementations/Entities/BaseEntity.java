package com.entheo.practice.tracker.Implementations.Entities;

import javax.persistence.*;

/**
 * Created by Jeremy on 11/11/2015.
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class BaseEntity extends Object{
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }
    protected boolean isReference() {
        return id!=null;
    }
}
