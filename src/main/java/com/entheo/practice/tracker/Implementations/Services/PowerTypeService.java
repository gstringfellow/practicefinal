package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Daos.PowerTypeDao;
import com.entheo.practice.tracker.Interfaces.Dao.IPowerTypeDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.PowerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service

public class PowerTypeService extends BaseService<PowerType> {

    @Autowired
    public PowerTypeService(IPowerTypeDao dao) {
        super(dao);
    }
}
