package com.entheo.practice.tracker.Implementations.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Entity
public class Origin extends BaseEntity {

    @Override
    public String toString() {
        return "Origin{" +
                "id='" + id + "\', " +
                "origin='" + origin + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Column(nullable = false)
    private String origin;
    private String description;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
