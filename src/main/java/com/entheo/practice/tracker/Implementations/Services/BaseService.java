package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;
import com.entheo.practice.tracker.Interfaces.IBaseCommands;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */

public abstract class BaseService<T extends BaseEntity> implements IBaseCommands<T> {

    protected IBaseDao dao;

    public BaseService(IBaseDao dao) {
        this.dao = dao;
    }

    public ArrayList<T> getAll() {
        return dao.getAll();
    }

    public void add(T t) {
        dao.add(t);
    }

    public void update(T t) {
        dao.update(t);
    }

    public void delete(Integer id) {

    }

    public BaseService() {

    }

    public T getByID(Integer id){
         return (T) dao.getByID(id);
    }
}
