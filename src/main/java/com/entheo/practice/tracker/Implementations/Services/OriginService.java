package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Interfaces.Dao.IOriginDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.Origin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service
public class OriginService extends BaseService<Origin> {
    @Autowired
    public OriginService(IOriginDao dao) {
        super(dao);
    }
}
