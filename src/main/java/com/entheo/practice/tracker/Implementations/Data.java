package com.entheo.practice.tracker.Implementations;

import com.entheo.practice.tracker.Implementations.Entities.*;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Jeremy on 11/21/2015.
 */
public class Data {
    static String[] HeroNames =    {"Superman",  "Batman",       "Green Lantern", "Robin",       "Flash"};
    static String[] MundaneNames = {"Clark Kent","Bruce Banner", "Simon Baz",     "Damian Wayne","Barry Allen"};
    static String[] Origins =      {"Krypton",   "A cave",       "Some green planet",       "An accident", "A laboratory"};
    static String[] Costumes = {"Red and Blue","Black","Green","Yellow red Green","Yellow red"};
    static String[][] Descriptions=  {{"Description1","Description3","Description4"},
            {"Description3","Description1","Description2"},
            {"Description2","Description3","Description6"},
            {"Description4","Description3","Description2"},
            {"Description1","Description5","Description4"}};
    static String[][] Powers = {{"Ice Beem","Heat Ray","Super Strength","Fly"},
            {"Bat mobile","Bat Wing","Baterang","Bat Cave","Bat Hook"},
            {"Ring"},{"Batman"},{"Super Speed","Laboratory"}};
    static  String[][] PowerTs= {{"Ray","Ray","Physical","Flight"},
            {"Vehicle","Vehicle","Weapon","Safe House","Tool"},{"Channeler"},
            {"Hero"},{"Physical","Safe House"}};
    static String[] Dems={"124,421","642,246","957,759","513,315","546,645"};
    public static ArrayList<Hero> getHeroes(){
        ArrayList<Hero> heros = new ArrayList<Hero>();

        for(int i = 0; i < HeroNames.length;i++)
        {

            Hero h = new Hero();
            h.setHeroName(HeroNames[i]);
            h.setMundaneName(MundaneNames[i]);

            Costume c = new Costume();
            c.setUrl(Costumes[i]);
            h.setCostume(c);

            Origin o = new Origin();
            o.setOrigin(Origins[i]);
            o.setDescription(Descriptions[i][1]);
            h.setOrigin(o);

            h.setWeight(new Integer(Dems[i].split(",")[0]));
            h.setHeight(new Integer(Dems[i].split(",")[1]));

            HashSet<Power> ps = new HashSet<Power>();
            for(int j = 0; j< (Powers[i]).length;j++){
                Power p = new Power();
                p.setName(Powers[i][j]);
                p.setDescription(Descriptions[i][2]);
                PowerType pt = new PowerType();
                pt.setType(Powers[i][j]);
                pt.setDescription(PowerTs[i][j]);
                p.setPowerType(pt);
                ps.add(p);
            }
            h.setPowers(ps);
            heros.add(h);
        }
        System.out.println(heros);
        return heros;
    }
}
