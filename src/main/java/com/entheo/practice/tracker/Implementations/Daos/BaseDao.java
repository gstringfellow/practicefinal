package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Data;
import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Interfaces.IPersistableChildren;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Transactional
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public abstract class BaseDao<T extends BaseEntity> implements IBaseDao<T> {

    public BaseDao(String table, Class<T> entity) {
        this.table = table;
        this.entity = entity;
    }

    @PersistenceContext
    protected EntityManager em;
    private String table;

    public void setEntity(Class<T> entity) {
        this.entity = entity;
    }

    public void setTable(String table) {
        this.table = table;
    }

    protected Class<T> entity;

    public BaseDao() {
    }

    public EntityManager getEm() {
        return em;
    }
    public void setEm(EntityManager em) {
        this.em = em;
    }

    private String getSelect(){
        return "SELECT x from  "+ table;
    }
    public T getByID(Integer id) {

        return em.createQuery(getSelect() +" x where x.id = :id", entity).setParameter("id",id).getSingleResult();
    }

    public  ArrayList<T> getAll() {
        //there is a bug wherein you cannot set the table with setParameter and must use string concat
        //http://stackoverflow.com/questions/18576864/table-name-as-parameter-in-hql
        return (ArrayList<T>) em.createQuery(getSelect() +" x", entity).getResultList();
    }
    private void PersistChildren(IPersistableChildren o){
        for(BaseEntity b : o.childrenToPersist()){
            if(b instanceof IPersistableChildren)
                PersistChildren((IPersistableChildren) b);
            em.persist(b);
        }
    }
    public void add(T t){
        if(t instanceof IPersistableChildren)
            PersistChildren((IPersistableChildren)t);
        System.out.println(em);
        em.persist(t);
    }
    public void update(T t){
        em.merge(t);
    }
    public void delete(Integer id){
        em.remove(getByID(id));
    }
}
