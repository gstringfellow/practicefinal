package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Daos.CostumeDao;
import com.entheo.practice.tracker.Interfaces.Dao.ICostumeDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.Costume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service
public class CostumeService extends BaseService<Costume>{
    @Autowired
    public CostumeService(ICostumeDao dao) {
        super(dao);
    }
}
