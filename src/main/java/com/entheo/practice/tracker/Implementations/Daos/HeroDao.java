package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Data;
import com.entheo.practice.tracker.Implementations.Entities.Hero;
import com.entheo.practice.tracker.Interfaces.Dao.IHeroDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
@Transactional
public class HeroDao extends BaseDao<Hero> implements IHeroDao {
    public HeroDao() {
        super("Hero",Hero.class);

           }

    public Hero getByHeroName(String heroName) {
        return em.createQuery("Select h from Hero h WHERE h.heroName = :heroName",entity).setParameter("heroName",heroName).getSingleResult();
    }

    public Hero getByMundaneName(String MundaneName) {
        return em.createQuery("Select h from Hero h WHERE h.mundaneName = :mundaneName",entity).setParameter("heroName",MundaneName).getSingleResult();
    }
}
