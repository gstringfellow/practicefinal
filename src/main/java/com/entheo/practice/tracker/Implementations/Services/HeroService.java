package com.entheo.practice.tracker.Implementations.Services;

import com.entheo.practice.tracker.Implementations.Daos.HeroDao;
import com.entheo.practice.tracker.Interfaces.Dao.IHeroDao;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.entheo.practice.tracker.Implementations.Entities.Hero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jeremy on 11/12/2015.
 */
@Service
public class HeroService extends BaseService<Hero> {

    private IHeroDao dao;
    @Autowired
    public HeroService(IHeroDao dao) {
        super(dao);
        this.dao = dao;
    }
    public Hero getByHeroName(String heroName){
        return dao.getByHeroName(heroName);
    }
    public Hero getByMundaneName(String mundaneName){
        return dao.getByMundaneName(mundaneName);
    }
}
