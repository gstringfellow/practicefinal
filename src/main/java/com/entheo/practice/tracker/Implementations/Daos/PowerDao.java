package com.entheo.practice.tracker.Implementations.Daos;

import com.entheo.practice.tracker.Implementations.Entities.Power;
import com.entheo.practice.tracker.Interfaces.Dao.IPowerDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by Jeremy on 11/11/2015.
 */
@Repository
@Transactional
public class PowerDao extends BaseDao<Power> implements IPowerDao{
    public PowerDao() {
        super("Power",Power.class);
    }
}
