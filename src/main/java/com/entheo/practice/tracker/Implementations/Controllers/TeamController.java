package com.entheo.practice.tracker.Implementations.Controllers;

import com.entheo.practice.tracker.Implementations.Entities.Team;
import com.entheo.practice.tracker.Implementations.Services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Jeremy on 11/11/2015.
 */
@RequestMapping(value = "/team")
@RestController
public class TeamController extends BaseController<Team> {
    @Autowired
    public TeamController(TeamService service) {
        super(service);
    }
}
