package com.entheo.practice.tracker;

import com.entheo.practice.tracker.Implementations.Data;
import com.entheo.practice.tracker.Implementations.Entities.Hero;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppRunner {
    public static void main(String[] args) {
        SpringApplication.run(com.entheo.practice.tracker.AppRunner.class);

    }
}
