package com.entheo.practice.tracker.Interfaces.Dao;

import com.entheo.practice.tracker.Implementations.Entities.PowerType;
import com.entheo.practice.tracker.Interfaces.IBaseDao;

/**
 * Created by Jeremy on 11/21/2015.
 */
public interface IPowerTypeDao extends IBaseDao<PowerType>{
}
