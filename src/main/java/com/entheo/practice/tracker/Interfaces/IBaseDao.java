package com.entheo.practice.tracker.Interfaces;

import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;

/**
 * Created by Jeremy on 11/16/2015.
 */
public interface IBaseDao<T extends BaseEntity> extends IBaseCommands<T> {
    public void setEntity(Class<T> entity);
    public void setTable(String table);
}
