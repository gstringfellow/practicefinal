package com.entheo.practice.tracker.Interfaces.Dao;

import com.entheo.practice.tracker.Implementations.Entities.Power;
import com.entheo.practice.tracker.Interfaces.IBaseDao;
import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;

/**
 * Created by Jeremy on 11/21/2015.
 */
public interface IPowerDao extends IBaseDao<Power>{
}
