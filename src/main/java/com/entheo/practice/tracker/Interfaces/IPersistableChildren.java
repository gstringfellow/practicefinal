package com.entheo.practice.tracker.Interfaces;

import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/14/2015.
 */
public interface IPersistableChildren {
    ArrayList<BaseEntity> childrenToPersist();

}
