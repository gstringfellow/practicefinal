package com.entheo.practice.tracker.Interfaces;

import com.entheo.practice.tracker.Implementations.Entities.BaseEntity;
import java.util.ArrayList;

/**
 * Created by Jeremy on 11/13/2015.
 */
public interface IBaseCommands<T extends BaseEntity> {
    T getByID(Integer id);
    ArrayList<T> getAll();
    void add(T t);
    void update(T t);
    void delete(Integer id);
}
