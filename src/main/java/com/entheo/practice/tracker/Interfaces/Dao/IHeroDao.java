package com.entheo.practice.tracker.Interfaces.Dao;

import com.entheo.practice.tracker.Implementations.Entities.Hero;
import com.entheo.practice.tracker.Interfaces.IBaseDao;

/**
 * Created by Jeremy on 11/21/2015.
 */
public interface IHeroDao extends IBaseDao<Hero> {
    Hero getByHeroName(String heroName);
    Hero getByMundaneName(String mundaneName);
}
